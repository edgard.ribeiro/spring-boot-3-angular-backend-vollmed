package br.com.med.voll.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.med.voll.domain.consultas.DadosAgendamentoConsulta;
import br.com.med.voll.domain.consultas.DadosCancelamentoConsulta;
import br.com.med.voll.domain.consultas.DadosDetalhamentoConsulta;
import br.com.med.voll.domain.consultas.validacoes.agendamento.ValidadorAgendamentoDeConsulta;
import br.com.med.voll.domain.consultas.validacoes.cancelamento.ValidadorCancelamentoDeConsulta;
import br.com.med.voll.entities.Consulta;
import br.com.med.voll.entities.Medico;
import br.com.med.voll.infra.exception.ValidacaoException;
import br.com.med.voll.repositories.ConsultaRepository;
import br.com.med.voll.repositories.MedicoRepository;
import br.com.med.voll.repositories.PacienteRepository;

@Service
public class AgendaDeConsultasService {
	
	@Autowired
	private ConsultaRepository consultaRepository;
	
	@Autowired
	private MedicoRepository medicoRepository;
	
	@Autowired
	private PacienteRepository pacienteRepository;
	
	@Autowired
	private List<ValidadorAgendamentoDeConsulta> validadores;
	
	@Autowired
	private List<ValidadorCancelamentoDeConsulta> validadoresCancelamento;
	
	public DadosDetalhamentoConsulta agendar(DadosAgendamentoConsulta dados) {
		if(!pacienteRepository.existsById(dados.idPaciente())) {
			throw new ValidacaoException("Id do paciente informado, não existe!");
		}
		
		if(dados.idMedico() != null && !medicoRepository.existsById(dados.idMedico())) {
			throw new ValidacaoException("Id do médico informado, não existe!");
		}
		
		validadores.forEach(v -> v.validar(dados));
		
		var paciente = pacienteRepository.getReferenceById(dados.idPaciente());
		var medico = escolherMedico(dados);
		if(medico == null) {
			throw new ValidacaoException("Não existe médico disponível nessa data!");
		}
		
		var consulta = new Consulta(null, medico, paciente, dados.data(), null);
		
		consultaRepository.save(consulta);
		
		return new DadosDetalhamentoConsulta(consulta);
	}	
	
	private Medico escolherMedico(DadosAgendamentoConsulta dados) {
		if(dados.idMedico() != null) {
			return medicoRepository.getReferenceById(dados.idMedico());
		}	
		
		if(dados.especialidade() == null) {
			throw new ValidacaoException("Especialidade é obrigatória quando não for escolhido!");
		}
		
		return medicoRepository.escolherMedicoAleatorioLivreNaData(dados.especialidade(), dados.data());
	}

	public void cancelar(DadosCancelamentoConsulta dados) {
		if (!consultaRepository.existsById(dados.idConsulta())) {
	        throw new ValidacaoException("Id da consulta informado não existe!");
	    }
		
		validadoresCancelamento.forEach(v -> v.validar(dados));

	    var consulta = consultaRepository.getReferenceById(dados.idConsulta());
	    consulta.cancelar(dados.motivo());		
	}
}
