package br.com.med.voll.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.med.voll.entities.Paciente;

public interface PacienteRepository extends JpaRepository<Paciente, Long>{
	
	Page<Paciente> findAllByAtivoTrue(Pageable paginacao);

	@Query("""
			SELECT p.ativo
			FROM Paciente p
			WHERE 
			p.id = :id
			""")
	Boolean findAtivoById(Long id);
}
