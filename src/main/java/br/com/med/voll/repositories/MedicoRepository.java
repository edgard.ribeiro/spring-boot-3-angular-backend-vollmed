package br.com.med.voll.repositories;

import java.time.LocalDateTime;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.med.voll.domain.medicos.Especialidade;
import br.com.med.voll.entities.Medico;

public interface MedicoRepository extends JpaRepository<Medico, Long>{
	
	Page<Medico> findAllByAtivoTrue(Pageable paginacao);

	@Query("""
			SELECT m FROM Medico m
			WHERE m.ativo = true
			and
			m.especialidade = :especialidade
			and
			m.id NOT IN(
				SELECT c.medico.id FROM Consulta c
				WHERE 
				c.data = :data
				and
				c.motivoCancelamento is null
			)
			ORDER BY RAND()
			limit 1		
			""")
	Medico escolherMedicoAleatorioLivreNaData(Especialidade especialidade, LocalDateTime data);

	@Query("""
			SELECT m.ativo
			FROM Medico m
			WHERE 
			m.id = :id
			""")
	Boolean findAtivoById(Long id);
}
