package br.com.med.voll.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.med.voll.domain.consultas.DadosAgendamentoConsulta;
import br.com.med.voll.domain.consultas.DadosCancelamentoConsulta;
import br.com.med.voll.services.AgendaDeConsultasService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/consultas")
@SecurityRequirement(name = "bearer-key")
public class ConsultaController {	
	
	@Autowired
	private AgendaDeConsultasService consultaService;

	 @PostMapping
	 @Transactional
	 public ResponseEntity agendar(@RequestBody @Valid DadosAgendamentoConsulta dados) {
	     var dto = consultaService.agendar(dados);
		 
	     return ResponseEntity.ok(dto);
	 }
	 
	 @DeleteMapping
	 @Transactional
	 public ResponseEntity cancelar(@RequestBody @Valid DadosCancelamentoConsulta dados) {
		 consultaService.cancelar(dados);
		 
		 return ResponseEntity.noContent().build();
	 }
}
