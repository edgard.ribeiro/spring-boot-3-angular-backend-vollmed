package br.com.med.voll.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.med.voll.domain.pacientes.DadosAtualizacaoPaciente;
import br.com.med.voll.domain.pacientes.DadosCadastroPaciente;
import br.com.med.voll.domain.pacientes.DadosDetalhamentoPaciente;
import br.com.med.voll.domain.pacientes.DadosListagemPaciente;
import br.com.med.voll.entities.Medico;
import br.com.med.voll.entities.Paciente;
import br.com.med.voll.repositories.PacienteRepository;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/pacientes")
@SecurityRequirement(name = "bearer-key")
public class PacienteController {
	
	@Autowired
	private PacienteRepository pacienteRepository;
	
	@PostMapping
	@Transactional
	public ResponseEntity<DadosDetalhamentoPaciente> cadastrar(@RequestBody @Valid DadosCadastroPaciente dados, UriComponentsBuilder uriBuilder) {
		var paciente = pacienteRepository.save(new Paciente(dados));
		var uri = uriBuilder.path("/pacientes/{id}").buildAndExpand(paciente.getId()).toUri();
		
		return ResponseEntity.created(uri).body(new DadosDetalhamentoPaciente(paciente));
	}
	
	@GetMapping
	public ResponseEntity<Page<DadosListagemPaciente>> listar(@PageableDefault(size = 10, sort = {"Nome"}) Pageable paginacao){
		var page = pacienteRepository.findAllByAtivoTrue(paginacao).map(DadosListagemPaciente::new);
		
		return ResponseEntity.ok().body(page);
	}
	
	@PutMapping
	@Transactional
	public ResponseEntity<DadosDetalhamentoPaciente> atualizar(@RequestBody @Valid DadosAtualizacaoPaciente dados) {
		var paciente = pacienteRepository.getReferenceById(dados.id());
		paciente.atualizarInformacoes(dados);		
		
		return ResponseEntity.ok(new DadosDetalhamentoPaciente(paciente));
	}
	
	@PutMapping("/reativar/{id}")
	@Transactional
    public ResponseEntity reativarPaciente(@PathVariable Long id) {
        var paciente = pacienteRepository.findById(id);
        if (paciente.isPresent()) {
            Paciente pacienteAtual = paciente.get();
            if (!pacienteAtual.isAtivo()) {
            	pacienteAtual.setAtivo(true);
                pacienteRepository.save(pacienteAtual);
                return ResponseEntity.ok().body("Paciente reativado com sucesso!");
            } else {
                return ResponseEntity.badRequest().body("Paciente já está ativo.");
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }
	
	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<Void> excluir(@PathVariable Long id) {
		//exclui o id inteiro no Banco de dados.
		//pacienteRepository.deleteById(id);
		var paciente = pacienteRepository.getReferenceById(id);
		paciente.excluir();
		
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/{id}")	
	public ResponseEntity<DadosDetalhamentoPaciente> detalhar(@PathVariable Long id) {		
		var paciente = pacienteRepository.getReferenceById(id);		
		
		return ResponseEntity.ok(new DadosDetalhamentoPaciente(paciente));
	}
}
