package br.com.med.voll.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.med.voll.domain.medicos.DadosAtualizacaoMedico;
import br.com.med.voll.domain.medicos.DadosCadastroMedico;
import br.com.med.voll.domain.medicos.DadosDetalhamentoMedico;
import br.com.med.voll.domain.medicos.DadosListagemMedico;
import br.com.med.voll.entities.Medico;
import br.com.med.voll.repositories.MedicoRepository;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/medicos")
@SecurityRequirement(name = "bearer-key")
public class MedicoController {
	
	@Autowired
	private MedicoRepository medicoRepository;
	
	@PostMapping
	@Transactional
	public ResponseEntity<DadosDetalhamentoMedico> cadastrar(@RequestBody @Valid DadosCadastroMedico dados, UriComponentsBuilder uriBuilder) {
		var medico = medicoRepository.save(new Medico(dados));
		var uri = uriBuilder.path("/medicos/{id}").buildAndExpand(medico.getId()).toUri();
		
		return ResponseEntity.created(uri).body(new DadosDetalhamentoMedico(medico));
	}
	
	@GetMapping
	public ResponseEntity<Page<DadosListagemMedico>> listar(@PageableDefault(size = 10, sort = {"Nome"}) Pageable paginacao){
		var page = medicoRepository.findAllByAtivoTrue(paginacao).map(DadosListagemMedico::new);
		
		return ResponseEntity.ok().body(page);
	}
	
	@PutMapping
	@Transactional
	public ResponseEntity<DadosDetalhamentoMedico> atualizar(@RequestBody @Valid DadosAtualizacaoMedico dados) {
		var medico = medicoRepository.getReferenceById(dados.id());		
		medico.atualizarInformacoes(dados);		
		
		return ResponseEntity.ok(new DadosDetalhamentoMedico(medico));
	}
	
	@PutMapping("/reativar/{id}")
	@Transactional
    public ResponseEntity reativarMedico(@PathVariable Long id) {
        var medico = medicoRepository.findById(id);
        if (medico.isPresent()) {
            Medico medicoAtual = medico.get();
            if (!medicoAtual.isAtivo()) {
                medicoAtual.setAtivo(true);
                medicoRepository.save(medicoAtual);
                return ResponseEntity.ok().body("Medico reativado com sucesso!");
            } else {
                return ResponseEntity.badRequest().body("Medico já está ativo.");
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }
	
	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<Void> excluir(@PathVariable Long id) {
		//exclui o id inteiro no Banco de dados.
		//medicoRepository.deleteById(id);
		var medico = medicoRepository.getReferenceById(id);
		medico.excluir();
		
		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/{id}")	
	public ResponseEntity<DadosDetalhamentoMedico> detalhar(@PathVariable Long id) {		
		var medico = medicoRepository.getReferenceById(id);		
		
		return ResponseEntity.ok(new DadosDetalhamentoMedico(medico));
	}
}
