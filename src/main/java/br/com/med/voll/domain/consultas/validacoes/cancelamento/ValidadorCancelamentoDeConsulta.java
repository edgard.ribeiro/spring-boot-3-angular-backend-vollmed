package br.com.med.voll.domain.consultas.validacoes.cancelamento;

import br.com.med.voll.domain.consultas.DadosCancelamentoConsulta;

public interface ValidadorCancelamentoDeConsulta {
	void validar(DadosCancelamentoConsulta dados);
}
