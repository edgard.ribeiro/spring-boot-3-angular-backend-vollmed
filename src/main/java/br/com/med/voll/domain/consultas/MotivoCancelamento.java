package br.com.med.voll.domain.consultas;

public enum MotivoCancelamento {
	PACIENTE_DESISTIU,
	MEDICO_CANCELOU,
	OUTROS
}
