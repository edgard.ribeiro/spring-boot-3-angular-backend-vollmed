package br.com.med.voll.domain.pacientes;

import br.com.med.voll.domain.enderecos.DadosEndereco;
import jakarta.validation.constraints.NotNull;

public record DadosAtualizacaoPaciente(
		@NotNull
		Long id, 
		String nome, 
		String telefone, 
		DadosEndereco endereco
		) {		

}
