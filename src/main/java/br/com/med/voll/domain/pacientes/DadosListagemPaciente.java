package br.com.med.voll.domain.pacientes;

import br.com.med.voll.entities.Paciente;

public record DadosListagemPaciente(Long id, String nome, String email, String cpf) {
	
	public DadosListagemPaciente(Paciente paciente) {
		this(paciente.getId(), paciente.getNome(), paciente.getEmail(), paciente.getCpf());
	}
}
