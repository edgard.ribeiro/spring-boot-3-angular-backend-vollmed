package br.com.med.voll.domain.medicos;

import br.com.med.voll.domain.enderecos.DadosEndereco;
import jakarta.validation.constraints.NotNull;

public record DadosAtualizacaoMedico(
		@NotNull
		Long id, 
		String nome, 
		String telefone, 
		DadosEndereco endereco
		) {		

}
