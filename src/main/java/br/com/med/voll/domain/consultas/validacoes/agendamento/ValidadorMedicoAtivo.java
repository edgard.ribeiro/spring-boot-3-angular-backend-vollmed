package br.com.med.voll.domain.consultas.validacoes.agendamento;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.med.voll.domain.consultas.DadosAgendamentoConsulta;
import br.com.med.voll.infra.exception.ValidacaoException;
import br.com.med.voll.repositories.MedicoRepository;

@Component
public class ValidadorMedicoAtivo implements ValidadorAgendamentoDeConsulta {
	
	@Autowired
	private MedicoRepository medicoRepository;
	
	public void validar(DadosAgendamentoConsulta dados) {
		//escolha do medico opcional
		if(dados.idMedico() == null) {
			return;
		}
		
		var medicoEstaAtivo = medicoRepository.findAtivoById(dados.idMedico());
		
		if(!medicoEstaAtivo) {
			throw new ValidacaoException("Consulta não pode ser agendada com médico excluído");
		}
		
	}

}
