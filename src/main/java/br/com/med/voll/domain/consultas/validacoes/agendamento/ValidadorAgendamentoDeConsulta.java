package br.com.med.voll.domain.consultas.validacoes.agendamento;

import br.com.med.voll.domain.consultas.DadosAgendamentoConsulta;

public interface ValidadorAgendamentoDeConsulta {
	
	void validar(DadosAgendamentoConsulta dados);

}
