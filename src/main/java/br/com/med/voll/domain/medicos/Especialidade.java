package br.com.med.voll.domain.medicos;

public enum Especialidade {
	ORTOPEDIA,
	CARDIOLOGIA,
	GINECOLOGIA,
	DERMATOLOGIA;
}
