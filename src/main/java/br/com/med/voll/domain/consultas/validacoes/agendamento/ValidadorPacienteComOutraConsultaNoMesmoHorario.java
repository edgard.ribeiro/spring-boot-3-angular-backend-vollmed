package br.com.med.voll.domain.consultas.validacoes.agendamento;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.med.voll.domain.consultas.DadosAgendamentoConsulta;
import br.com.med.voll.infra.exception.ValidacaoException;
import br.com.med.voll.repositories.ConsultaRepository;

@Component
public class ValidadorPacienteComOutraConsultaNoMesmoHorario implements ValidadorAgendamentoDeConsulta {
	
	@Autowired
	private ConsultaRepository consultaRepository;
	
	public void validar(DadosAgendamentoConsulta dados) {
		var primeiroHorario = dados.data().withHour(7);
		var ultimoHorario = dados.data().withHour(18);
		var pacientePossuiOutraConsultaNoMesmoHorario = consultaRepository.existsByPacienteIdAndDataBetween(dados.idPaciente(), primeiroHorario, ultimoHorario);
		
		if(pacientePossuiOutraConsultaNoMesmoHorario) {
			throw new ValidacaoException("Paciente já possui uma consulta agendada nesse dia");
		}
	}
}
