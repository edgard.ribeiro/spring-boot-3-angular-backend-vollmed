package br.com.med.voll.domain.usuario;

public record DadosAutenticacao(String login, String senha) {

}
