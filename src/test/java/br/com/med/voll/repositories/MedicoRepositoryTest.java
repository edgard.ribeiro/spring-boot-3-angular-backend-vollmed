package br.com.med.voll.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

import br.com.med.voll.domain.enderecos.DadosEndereco;
import br.com.med.voll.domain.medicos.DadosCadastroMedico;
import br.com.med.voll.domain.medicos.Especialidade;
import br.com.med.voll.domain.pacientes.DadosCadastroPaciente;
import br.com.med.voll.entities.Consulta;
import br.com.med.voll.entities.Medico;
import br.com.med.voll.entities.Paciente;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
public class MedicoRepositoryTest {
	
	@Autowired
	private MedicoRepository medicoRepository;
	
	@Autowired
	private TestEntityManager em;
	
	@Test
	@DisplayName("Deveria devolver null quando unico medico cadastrado nao esta disponivel na data")
	void escolherMedicoAleatorioLivreNaDataCenario1() {
		
		//Given ou arrange
		var proximaSegundaAs10 = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.MONDAY)).atTime(10, 0);
		
		var medico = cadastrarMedico("Sophie", "sophie@vollmed.com.br", "123456", Especialidade.CARDIOLOGIA);
		var paciente = cadastrarPaciente("Amanda", "amanda.avelina16@gmail.com", "123.456.789-98");
	    var consulta = cadastrarConsulta(medico, paciente, proximaSegundaAs10);
		
	    //When ou act
		var medicoLivre = medicoRepository.escolherMedicoAleatorioLivreNaData(Especialidade.CARDIOLOGIA, proximaSegundaAs10);
		
		//Then ou assert
		assertThat(medicoLivre).isNull();		
	}
	
	@Test
	@DisplayName("Deveria devolver medico quando ele estiver disponivel na data")
	void escolherMedicoAleatorioLivreNaDataCenario2() {
		
		//Given ou arrange
		var proximaSegundaAs10 = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.MONDAY)).atTime(10, 0);
				
		var medico = cadastrarMedico("Sophie", "sophie@vollmed.com.br", "123456", Especialidade.CARDIOLOGIA);
		
		//When ou act
		var medicoLivre = medicoRepository.escolherMedicoAleatorioLivreNaData(Especialidade.CARDIOLOGIA, proximaSegundaAs10);
		
		//Then ou assert
		assertThat(medicoLivre).isEqualTo(medico);		
	}
	
	private Consulta cadastrarConsulta(Medico medico, Paciente paciente, LocalDateTime data) {
		var consulta = new Consulta(null, medico, paciente, data, null);
	    em.persist(consulta);
	    return consulta;
	}

	private Medico cadastrarMedico(String nome, String email, String crm, Especialidade especialidade) {
	    var medico = new Medico(dadosMedico(nome, email, crm, especialidade));
	    em.persist(medico);
	    return medico;
	}

	private Paciente cadastrarPaciente(String nome, String email, String cpf) {
	    var paciente = new Paciente(dadosPaciente(nome, email, cpf));
	    em.persist(paciente);
	    return paciente;
	}

	private DadosCadastroMedico dadosMedico(String nome, String email, String crm, Especialidade especialidade) {
	    return new DadosCadastroMedico(
	            nome,
	            email,
	            "61999999999",
	            crm,
	            especialidade,
	            dadosEndereco()
	    );
	}

	private DadosCadastroPaciente dadosPaciente(String nome, String email, String cpf) {
	    return new DadosCadastroPaciente(
	            nome,
	            email,
	            "61999999999",
	            cpf,
	            dadosEndereco()
	    );
	}

	private DadosEndereco dadosEndereco() {
	    return new DadosEndereco(
	            "rua xpto",
	            "bairro",
	            "00000000",
	            "Brasilia",
	            "DF",
	            null,
	            null
	    );
	}
}
