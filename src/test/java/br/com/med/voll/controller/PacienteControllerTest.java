package br.com.med.voll.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import br.com.med.voll.domain.enderecos.DadosEndereco;
import br.com.med.voll.domain.pacientes.DadosCadastroPaciente;
import br.com.med.voll.domain.pacientes.DadosDetalhamentoPaciente;
import br.com.med.voll.entities.Endereco;
import br.com.med.voll.entities.Paciente;
import br.com.med.voll.repositories.PacienteRepository;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureJsonTesters
public class PacienteControllerTest {
	
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private JacksonTester<DadosCadastroPaciente> dadosCadastroPacienteJson;
	
	@Autowired
    private JacksonTester<DadosDetalhamentoPaciente> dadosDetalhamentoPacienteJson;
	
	@MockBean
	private PacienteRepository pacienteRepository;
	
	@Test
	@DisplayName("Deveria devolver codigo http 400 quando informacoes estao invalidas")
	@WithMockUser
	void cadastrarCenario1() throws Exception {
		var response = mvc.perform(post("/pacientes")).andReturn().getResponse();
		assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
	}
	
	@Test
	@DisplayName("Deveria devolver codigo http 200 quando informacoes estao validas")
	@WithMockUser
	void cadastrarCenario2() throws Exception {
		var dadosCadastro = new DadosCadastroPaciente("Sophie Avelina Moncao Ribeiro", "sophie.moncao@gmail.com", "61994456187", "290.423.640-68", dadosEndereco());
		
		when(pacienteRepository.save(any())).thenReturn(new Paciente(dadosCadastro));
		
		var response = mvc.perform(post("/pacientes")
				.contentType(MediaType.APPLICATION_JSON)
				.content(dadosCadastroPacienteJson.write(dadosCadastro).getJson()))
		.andReturn().getResponse();
		
		var dadosDetalhamento = new DadosDetalhamentoPaciente(
				null,
				dadosCadastro.nome(),
				dadosCadastro.email(),				
				dadosCadastro.cpf(),
				dadosCadastro.telefone(),
				new Endereco(dadosCadastro.endereco()));
		
		assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
		
		var jsonEsperado = dadosDetalhamentoPacienteJson.write(dadosDetalhamento).getJson();
		
		assertThat(response.getContentAsString()).isEqualTo(jsonEsperado);
	}

	private DadosEndereco dadosEndereco() {
		
		return new DadosEndereco(
	            "rua xpto",
	            "bairro",
	            "00000000",
	            "Brasilia",
	            "DF",
	            null,
	            null
	    );
	}
}
